pHash
=====

The open source perceptual hash library
---------------------------------------

This is a fork of the [pHash](https://www.phash.org/) library and tools.
The goal here is to change the build system from
[GNU Autoconf](https://www.gnu.org/software/autoconf/autoconf.html)
to [CMake](https://cmake.org/).

Secondary goals are to move the library to modern C++ (11/14/17/21...) where
appropriate

Tertiary goals are to restore bindings to scripting languages

---

TODO

- [X] Create stub CMake build system
- [X] Add Image support
- [X] Add Video support
- [X] Add Audio support
- [X] Build text examples
- [X] Build image examples
- [X] Build audio examples
- [X] Build video examples
- [X] Remove platform specific build requirements, when possible.
- [X] Verify Linux build
- [X] Verify MacOS X Build
- [ ] Verify Windows build
- [ ] Eliminate build errors and warnings (gcc, clang and MSVC w/ default settings)

---

Future work

- [ ] Restore bindings to PHP, Java and C#
- [ ] Port code from pthread to c++11 std::thread
- [ ] Explore moving to [fftw](http://www.fftw.org/)
- [ ] Refactor memory management to modern C++
